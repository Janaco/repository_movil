﻿using System;
using System.IO;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Droid.Services;
using Proyecto_Ranbach.Interface;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidSQLitePlatform))]
namespace Proyecto_Ranbach.Droid.Services
{
    public class AndroidSQLitePlatform: ISQLitePlatform
    {
        private string GetPath()
        {
            var dbName = ApiClient.nameDB;
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
 
            return path;
        }

        public SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(GetPath());
        }
        public SQLiteAsyncConnection GetConnectionAsync()
        {
            return new SQLiteAsyncConnection(GetPath());
        }
    }
}
