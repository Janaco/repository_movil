﻿using System;
using Android.App;
using Android.Widget;
using Proyecto_Ranbach.Droid.Services;
using Proyecto_Ranbach.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(MessageAndroid))]
namespace Proyecto_Ranbach.Droid.Services
{
    public class MessageAndroid : IMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}
