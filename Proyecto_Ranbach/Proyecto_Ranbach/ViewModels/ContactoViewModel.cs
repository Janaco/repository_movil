﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Proyecto_Ranbach.Views.Contactos;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class ContactoViewModel: BaseViewModel
    {
        vw_contactos Page;
        readonly IList<ContactoModel> lista;

        public INavigation navigation { get; set; }
        public ICommand AgregarCmmd { get; set; }
        public ICommand ChatCmmd { get; set; }
        private ObservableCollection<ContactoModel> _contactos;
 
        public ObservableCollection<ContactoModel> Contactos
        {
            get { return _contactos; }
            set { _contactos = value; OnPropertyChanged("Contactos"); }
        }

        public ContactoViewModel(INavigation _navigation, vw_contactos _page)
        {

            lista = new List<ContactoModel>();
            Initialize();
            navigation = _navigation;
            Page = _page;
            AgregarCmmd = new Command(async () => await Agregar());
            //ChatCmmd = new Command<ContactoModel>(async (data) => await Chat(data)); 
        }

        private async void Initialize()
        { 
            await Listar();
            Contactos = new ObservableCollection<ContactoModel>(lista);
        }
        
        async Task Agregar()
        {
            try {
                string result = await Page.DisplayPromptAsync(Constante.buscar, Constante.nombreUsuario, Constante.aceptar, Constante.cancelar);
                if (string.IsNullOrEmpty(result) == false)
                {

                    //1. MOSTRAR CIRCULITO CARGANDO BUSQUEDA
                    var resultado = await UsuarioWs.ObtenerUsuarioxNombreUsuario(result);

                    //2. Devolver mensaje si el usuario fue encontrado o no
                    if (resultado != null)
                    {
                        //3. Agregar a su base de datos de contactos                    

                        var item = new ContactoModel()
                        {
                            NombreContacto = result,
                            IdUsuario = App.usuario.IdUsuario,
                            IdUsuarioxContacto = resultado.usuario.IdUsuario,
                            Estado = 1
                        };
                        var resultado1 = await UsuarioWs.AgregarContacto(item);

                        if (resultado1.codigo.Equals(0))
                        {
                            if (Contactos == null)
                                Contactos = new ObservableCollection<ContactoModel>();

                            //Contactos.Add(item);
                            await Listar();
                        }
                    }
                    else
                    {
                        Util.LongAlerta(Mensaje.usuarioNoDisponible); 
                    }
                }
            }
            catch (Exception e) {
                Util.LongAlerta(Mensaje.Error);
            }
            
        }

        async Task Listar()
        {
            try {
                var resultado2 = await UsuarioWs.ListarContacto(App.usuario.IdUsuario);
                if(resultado2.listaContacto != null)
                    if (resultado2.listaContacto.Count > 0)
                    {
                        foreach (var r in resultado2.listaContacto)
                            lista.Add(r);
                    }

                Contactos = new ObservableCollection<ContactoModel>(lista);

            }
            catch(Exception ex)
            { 
                Util.LongAlerta(Mensaje.Error);
            }
        }
    }
}
