﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class PrincipalViewModel: BaseViewModel
    {
        public UsuarioModel usuario { get; set; }
        public INavigation navigation { get; set; }

        public ICommand CompraCmmd { get; set; }
        public ICommand JuegoCmmd { get; set; }
        public ICommand InicioCmmd { get; set; }
        public ICommand ChatCmmd { get; set; }
        public ICommand ConfigurationCmmd { get; set; }

        public PrincipalViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            usuario = App.usuario;
            
            CompraCmmd = new Command(async () => await Compra());
            JuegoCmmd = new Command(async () => await Juego());
            InicioCmmd = new Command(async () => await Inicio());
            ChatCmmd = new Command(async () => await Chat());
            ConfigurationCmmd = new Command(async () => await Configuration());
        }

        async Task Compra()
        {
            try {
                await navigation.PushAsync(new VW_Compra());
            }
            catch (Exception ex) {

            }
        }
        async Task Juego()
        {
            try
            {
                //await navigation.PushAsync(new vw_juegos());
                await navigation.PushAsync(new VW_Juegos2() );
            }
            catch (Exception ex) { }
        }
        async Task Inicio()
        {
            try
            {
            }
            catch (Exception ex) { }
        } 
        async Task Chat()
        {
            try
            {
                await navigation.PushAsync(new vw_contactos());
            }
            catch (Exception ex) { }
        } 
        async Task Configuration()
        {
            try
            {
                await navigation.PushAsync(new vw_config());
            }
            catch (Exception ex) { }
        }
    }
}
