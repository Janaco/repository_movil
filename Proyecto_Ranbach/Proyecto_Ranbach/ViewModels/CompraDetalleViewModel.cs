﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Models;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class CompraDetalleViewModel: BaseViewModel
    {
        
        public INavigation navigation { get; set; }

        public ICommand TokenCmmd { get; set; }
        public ObservableCollection<ProductoModel> productoList { get; set; }

        public CompraDetalleViewModel(INavigation _navigation, int _identity)
        {
            navigation = _navigation;

            ObtenerProducto(_identity);

            TokenCmmd = new Command(async () => await Compra());
        }

        async Task ObtenerProducto(int categoria)
        {
            try {
                string IdCategoria = categoria.ToString();
                productoList = await ProductoWs.ObtenerProductoxCategoria(IdCategoria);
            }
            catch (Exception ex) {
                Debug.Print("ObtenerProducto: {0}", ex.Message); 
            }
        }

        async Task Compra()
        {
            try{

            }catch(Exception ex) { }
        }
    }
}
