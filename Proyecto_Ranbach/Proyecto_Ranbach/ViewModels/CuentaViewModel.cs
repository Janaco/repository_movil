﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Media;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Views;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class CuentaViewModel: BaseViewModel
    {
        private int identity;
        private bool _verGuardar = true;

        public bool VerGuardar
        {
            get { return _verGuardar; }
            set { _verGuardar = value; OnPropertyChanged("VerGuardar"); }
        }

        public ICommand FotoCmmd { get; set; }
        public ICommand GaleriaCmmd { get; set; }
        public ICommand GuardaCmmd { get; set; }

        public CuentaViewModel(int _identity)
        {
            identity = _identity;
            if (identity == 1)
                VerGuardar = false;

            FotoCmmd = new Command(async () => await Foto());
            GaleriaCmmd = new Command(async () => await Galeria());
            GuardaCmmd = new Command(async () => await Guardar());
        }
        async Task Foto()
        {
            try {
                await CrossMedia.Current.Initialize();
                if (await Util.CheckCamera())
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(
                        new Plugin.Media.Abstractions.StoreCameraMediaOptions
                        {
                            Directory ="Gdas",
                            Name=DateTime.Now.Date.ToString()+"_"+DateTime.Now.ToShortTimeString()+".JPG",
                            SaveToAlbum =true,
                            CompressionQuality = 75,
                            CustomPhotoSize =50,
                            PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
                            MaxWidthHeight = 2000,
                            DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
                        });

                    if (file == null) return;

                    Util.LongAlerta(file.Path);
                }
            }
            catch (Exception ex) {
                Debug.Print("Foto: " + ex.Message);
            }
        }
        async Task Galeria()
        {
            try {
                if (await Util.CheckGalery())
                {
                    var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(
                        new Plugin.Media.Abstractions.PickMediaOptions{
                            PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
                    });

                    if (file == null) return;
                }
            }
            catch (Exception ex) {
                Debug.Print("Galeria: " + ex.Message);
            }
        }
        async Task Guardar()
        {
            App.Current.MainPage = new NavigationPage(new VW_Principal());
        }
    }
}
