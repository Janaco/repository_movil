﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Services;
using Proyecto_Ranbach.Helpers;
using Proyecto_Ranbach.Interface;

namespace Proyecto_Ranbach.ViewModels
{
    public class BaseViewModel : ObservableObject
    {
        public IUsuarioWebService<UsuarioModel> UsuarioWs => DependencyService.Get<IUsuarioWebService<UsuarioModel>>();

        public IProductoWebService<ProductoModel> ProductoWs => DependencyService.Get<IProductoWebService<ProductoModel>>();
        public IChatService<ChatModel> ChatWs => DependencyService.Get<IChatService<ChatModel>>();

        public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }
         
    }
}
