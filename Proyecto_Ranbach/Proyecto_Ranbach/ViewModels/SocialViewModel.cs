﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.GoogleClient.Shared;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Interface;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class SocialViewModel : BaseViewModel
    {

        IFacebookClient FacebookService = CrossFacebookClient.Current;
        IGoogleClientManager GoogleService = CrossGoogleClient.Current;
        IOAuth2Service OAuth2Service;

        public ICommand CerrarCmmd { get; set; }
        public ICommand CorreoCmmd { get; set; }
        public ICommand FacebookCmmd { get; set; }
        public ICommand GoogleCmmd { get; set; }

        public SocialViewModel()
        {
            CorreoCmmd = new Command(async ()=> await Correo());
            FacebookCmmd = new Command(async ()=> await Facebook());
            GoogleCmmd = new Command(async ()=> await Google());
            CerrarCmmd = new Command(async () => await Cerrar());
        }


        async Task Correo()
        {
            //App.Current.MainPage = new VW_Registro();
            await PopupNavigation.PushAsync(new VW_Registro());
        }
        async Task Cerrar()
        {
            //App.Current.MainPage = new VW_Registro();
            await PopupNavigation.PopAllAsync(true);
        }
        async Task Facebook()
        {
            try
            {
                AuthNetwork authNetwork = new AuthNetwork();
                authNetwork.Name = "Facebook";
                authNetwork.Icon = "facebook";
                authNetwork.Foreground = "#FFFFFF";
                authNetwork.Background = "#4768AD";


                if (FacebookService.IsLoggedIn)
                {
                    FacebookService.Logout();
                }

                EventHandler<FBEventArgs<string>> userDataDelegate = null;

                userDataDelegate = async (object sender, FBEventArgs<string> e) =>
                {
                    switch (e.Status)
                    {
                        case FacebookActionStatus.Completed:
                            var facebookProfile = await Task.Run(() => JsonConvert.DeserializeObject<FacebookProfile>(e.Data));
                            var socialLoginData = new NetworkAuthData
                            {
                                Id = facebookProfile.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = facebookProfile.Picture.Data.Url,
                                Name = $"{facebookProfile.FirstName} {facebookProfile.LastName}",
                                Password = "1",
                                Correo = facebookProfile.Email,
                            };
                            //await App.Current.MainPage.Navigation.PushModalAsync(new InicioPage(socialLoginData));
                            App.Current.MainPage = new VW_Login();
                            break;
                        case FacebookActionStatus.Canceled:
                            Debug.Print("Cancelar");
                            //await App.Current.MainPage.DisplayAlert("Facebook Auth", "Canceled", "Ok");
                            //Util.LongAlerta(Mensaje.Tarde);
                            break;
                        case FacebookActionStatus.Error:
                            //await App.Current.MainPage.DisplayAlert("Facebook Auth", "Error", "Ok");
                            Util.LongAlerta(Mensaje.Error);
                            break;
                        case FacebookActionStatus.Unauthorized:
                            //await App.Current.MainPage.DisplayAlert("Facebook Auth", "Unauthorized", "Ok");
                            Util.LongAlerta(Mensaje.Error);
                            break;
                    }

                    FacebookService.OnUserData -= userDataDelegate;
                };

                FacebookService.OnUserData += userDataDelegate;

                string[] fbRequestFields = { "email", "first_name", "picture", "gender", "last_name" };
                string[] fbPermisions = { "email" };
                await FacebookService.RequestUserDataAsync(fbRequestFields, fbPermisions);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"ERROR: Facebook, Mensaje: " + ex.Message.ToString());
                Util.LongAlerta(Mensaje.Error);
            }
        }

        async Task Google()
        {
            try
            {
                AuthNetwork authNetwork = new AuthNetwork();
                authNetwork.Name = "Google";
                authNetwork.Icon = "google";
                authNetwork.Foreground = "#FFFFFF";
                authNetwork.Background = "#e04c34";

                if (!string.IsNullOrEmpty(GoogleService.AccessToken))
                {
                    //Always require user authentication
                    GoogleService.Logout();
                }

                EventHandler<GoogleClientResultEventArgs<GoogleUser>> userLoginDelegate = null;
                userLoginDelegate = async (object sender, GoogleClientResultEventArgs<GoogleUser> e) =>
                {
                    switch (e.Status)
                    {
                        case GoogleActionStatus.Completed:
#if DEBUG
                            var googleUserString = JsonConvert.SerializeObject(e.Data);
                            Debug.WriteLine($"Google Logged in succesfully: {googleUserString}");
#endif
                            
                            var socialLoginData = new NetworkAuthData
                            {
                                Id = e.Data.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = e.Data.Picture.AbsoluteUri,
                                Name = e.Data.Name,
                                Password = "1",
                                Correo = e.Data.Email,
                            }; 
                            var socialLoginData1 = new UsuarioModel
                            { 
                                NombreUsuario = e.Data.Name,
                                Contrasenia = "1",
                                IdTipoDocumento= 1,
                                NroDocumento = e.Data.Email,
                                 
                            };

                            //await App.Current.MainPage.Navigation.PushModalAsync(new InicioPage(socialLoginData));
                            App.Current.MainPage = new VW_Login();
                            break;
                        case GoogleActionStatus.Canceled:
                            Debug.Print("Cancelar");
                            //await App.Current.MainPage.DisplayAlert("Google Auth", "Canceled", "Ok");
                            //Util.LongAlerta(Mensaje.Tarde);
                            break;
                        case GoogleActionStatus.Error:
                            //await App.Current.MainPage.DisplayAlert("Google Auth", "Error", "Ok");
                            Util.LongAlerta(Mensaje.Error);
                            break;
                        case GoogleActionStatus.Unauthorized:
                            //await App.Current.MainPage.DisplayAlert("Google Auth", "Unauthorized", "Ok");
                            Util.LongAlerta(Mensaje.Error);
                            break;
                    }

                    GoogleService.OnLogin -= userLoginDelegate;
                };

                GoogleService.OnLogin += userLoginDelegate;

                try
                {
                    await GoogleService.LoginAsync();
                }
                catch (Exception ex)
                {

                }
                //await GoogleService.LoginAsync();
            }
            catch (Exception ex)
            {

                if (ex.InnerException != null)
                {
                    Debug.WriteLine($"ERROR: Google, Mensaje: " + ex.InnerException.ToString());
                }
                Debug.WriteLine($"ERROR: Google, Mensaje: " + ex.Message.ToString());

                Util.LongAlerta(Mensaje.Error);
            }
        }
    }
}
