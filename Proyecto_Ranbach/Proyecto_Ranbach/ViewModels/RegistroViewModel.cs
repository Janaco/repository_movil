﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Services;
using Proyecto_Ranbach.Views;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class RegistroViewModel: BaseViewModel
    {
        string msgValue = "";

        public UsuarioModel usuario { get; set; }

        public ICommand AtrasCmmd { get; set; }
        public ICommand CerrarCmmd { get; set; }
        public ICommand GuardarCmmd { get; set; }
        public ICommand RegresarCmmd { get; set; }

        public RegistroViewModel(UsuarioModel _usuario)
        {
            usuario = _usuario; 
            GuardarCmmd = new Command(async () => await Guardar());
            RegresarCmmd = new Command(async () => await Regresar());
            AtrasCmmd = new Command(async () => await Atras());
            CerrarCmmd = new Command(async () => await Cerrar());
        }

        async Task Regresar()
        {
            try {
                App.Current.MainPage = new VW_Login();
            }
            catch (Exception ex) { }
        }
        async Task Cerrar()
        {
            try
            {
                await PopupNavigation.PopAllAsync(true);
            }
            catch (Exception ex) { }
        }
        async Task Atras()
        {
            try
            {
                await PopupNavigation.PopAsync(true);
            }
            catch (Exception ex) { }
        }
        async Task Guardar()
        {
            try {
                if (ValidaDato())
                {
                    usuario.IdTipoDocumento = 1;
                    if (Util.CheckForInternetConnection())
                    {
                        var data = await UsuarioWs.RegistrarUsuario(usuario, true);

                        if (data.codigo.Equals(0) && data.success)
                        {
                            App.Current.MainPage = new VW_Login();
                        }
                        else
                        {
                            Util.LongAlerta(data.mensaje);
                        }
                    }
                }
                else{
                    Util.ShortAlerta(msgValue);
                }
            }
            catch (Exception ex) {
                Debug.Print("Registro: " + ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
        }

        bool ValidaDato()
        {
            msgValue = "";
            if (string.IsNullOrEmpty(usuario.NombreUsuario))
            {
                msgValue = Mensaje.usuario;
                return false;
            } 

            if (string.IsNullOrEmpty(usuario.Correo))
            {
                msgValue = Mensaje.correo;
                return false;
            } 
            if (string.IsNullOrEmpty(usuario.Contrasenia))
            {
                msgValue = Mensaje.contrasenia;
                return false;
            }
            if (string.IsNullOrEmpty(usuario.NroDocumento))
            {
                msgValue = Mensaje.dni;
                return false;
            }
            return true;
        }
    }
}
