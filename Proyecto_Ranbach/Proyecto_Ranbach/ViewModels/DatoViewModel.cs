﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class DatoViewModel: BaseViewModel
    {
        string msgValue = "";

        public UsuarioModel usuario { get; set; }

        public INavigation navigation { get; set; }
        public ICommand AvatarCmmd { get; set; }
        public ICommand GuardarCmmd { get; set; }

        public DatoViewModel(INavigation _navigation)
        {
            usuario = App.usuario;
            navigation = _navigation;

            AvatarCmmd = new Command(async () => await Avatar());
            GuardarCmmd = new Command(async () => await Guardar());
        }

        async Task Guardar()
        {
            try
            {
                if (ValidaDato()){
                    if (Util.CheckForInternetConnection())
                    {
                        var resultado = await UsuarioWs.RegistrarUsuario(usuario, false);
                    }
                    else Util.ShortAlerta(msgValue);
                }
            }
            catch (Exception ex) { }
        }

        async Task Avatar()
        {
            try {

                await navigation.PushAsync(new VW_Cuenta(1));
            }
            catch (Exception ex) { }
        }

        bool ValidaDato()
        {
                msgValue = "";
                if (string.IsNullOrEmpty(usuario.Nombre))
                {
                    msgValue = Mensaje.usuario;
                    return false;
                }

                if (string.IsNullOrEmpty(usuario.Apellido))
                {
                    msgValue = Mensaje.apellidos;
                    return false;
                }

                if (string.IsNullOrEmpty(usuario.Correo))
                {
                    msgValue = Mensaje.correo;
                    return false;
                }
                if (string.IsNullOrEmpty(usuario.CorreoAlternativo))
                {
                    msgValue = Mensaje.correoAlternativo;
                    return false;
                }
                if (string.IsNullOrEmpty(usuario.Contrasenia))
                {
                    msgValue = Mensaje.contrasenia;
                    return false;
                }
                if (string.IsNullOrEmpty(usuario.NroDocumento))
                {
                    msgValue = Mensaje.dni;
                    return false;
                }
                return true;
            
        }
    }
}
