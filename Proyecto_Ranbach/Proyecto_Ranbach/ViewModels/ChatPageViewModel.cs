﻿using Microsoft.AspNetCore.SignalR.Client;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Enum;
using Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class ChatPageViewModel : BaseViewModel
    {
        public bool ShowScrollTap { get; set; } = false;
        public bool LastMessageVisible { get; set; } = true;
        public int PendingMessageCount { get; set; } = 0;
        public bool PendingMessageCountVisible { get { return PendingMessageCount > 0; } }

        public Queue<Message> DelayedMessages { get; set; } = new Queue<Message>();
        public ObservableCollection<Message> Messages { get; set; } = new ObservableCollection<Message>();
        public string TextToSend { get; set; }
        public ICommand OnSendCommand { get; set; }
        public ICommand MessageAppearingCommand { get; set; }
        public ICommand MessageDisappearingCommand { get; set; }

        HubConnection hubConnection;
        ContactoModel obj;


        public ChatPageViewModel(ContactoModel item )
        {
            obj = item;
            //LEER MENSAJES EN BD
            obtenerMensajesGuardados();

            Debug.Print("URI ChatHub -> "+ApiClient.chathub);

            hubConnection = new HubConnectionBuilder()
                .WithUrl(ApiClient.chathub, options =>
            {
                options.AccessTokenProvider = () => Task.FromResult(App.usuario.AccessToken);
            })
            .Build();

            /*
            hubConnection.On<string>(Constante.receivePrivateMessage, async (message) =>
            {
                Messages.Insert(0, new Message() { Text = message });
                var objMensaje = construirMensajeDatabaseModel((int)Comunicador.Receptor, message);
                var result = await ChatWs.AgregarAsync(objMensaje);
                Console.WriteLine("INSERTO RECEPTOR: " + result);
            }); */
            /*
            OnSendCommand = new Command(async () =>
            {
                if (!string.IsNullOrEmpty(TextToSend))
                {
                    Messages.Insert(0, new Message() { Text = TextToSend, User = App.emisor });
                    await SendMessage(obj.IdUsuario.ToString(), TextToSend);
                    //TextToSend = string.Empty;

                }

            }); */
            hubConnection.On<string>(Constante.receivePrivateMessage, async (message) => await hubMethod(message));
            Connect();
             
            OnSendCommand = new Command<Message>(async (data) => await SendMessage(data));

        }

        async Task obtenerMensajesGuardados()
        {
            try {

                var fechaInicial = DateTime.Now.AddMonths(-1);
                var listaMensajes = await ChatWs.ObtenerAsync(obj, fechaInicial, DateTime.Now);
                foreach (var mensaje in listaMensajes)
                {
                    //var comunicador = ;
                    Messages.Insert(0, new Message() { Text = mensaje.Mensaje, User = mensaje.Comunicador.ToString() });
                }
            }
            catch (Exception e) { } 
        }

        async Task Connect()
        {
            try
            {
                await hubConnection.StartAsync();
            }
            catch (Exception ex)
            {
                var msj = ex.Message;
            }
        }

        public async Task Disconnect()
        {
            try
            {
                await hubConnection.StopAsync();

            }
            catch (Exception ex)
            {

                var msj = ex.Message;
            }
        }

        async Task SendMessage(Message data)
        {
            try
            {
                if (!string.IsNullOrEmpty(TextToSend))
                {
                    Messages.Insert(0, new Message() { Text = TextToSend, User = App.emisor });
                    await hubConnection.InvokeAsync(Constante.sendPrivateMessage, data.User, data.Text);
                    var objMensaje = construirMensajeDatabaseModel((int)Comunicador.Emisor, data.Text);
                    var result = await ChatWs.AgregarAsync(objMensaje);
                    Console.WriteLine("INSERTO EMISOR: " + result);
                    //TextToSend = string.Empty;

                }
                /*
                await hubConnection.InvokeAsync(Constante.sendPrivateMessage, user, message);
                var objMensaje = construirMensajeDatabaseModel((int)Comunicador.Emisor, message);
                var result = await ChatWs.AgregarAsync(objMensaje);
                Console.WriteLine("INSERTO EMISOR: " + result); */
            }
            catch (Exception ex)
            {
                int stop = 0;
            }
        }

        ChatModel construirMensajeDatabaseModel(int comunicador, string mensaje)
        {
            return new ChatModel()
            {
                IdLogin = App.usuario.IdUsuario,
                IdContacto = obj.IdUsuarioxContacto,
                Comunicador = comunicador,
                Mensaje = mensaje,
                Fecha = DateTime.Now
            };
        }

        async Task hubMethod(string mensaje)
        {
            Messages.Insert(0, new Message() { Text = mensaje });
            var objMensaje = construirMensajeDatabaseModel((int)Comunicador.Receptor, mensaje);
            var result = await ChatWs.AgregarAsync(objMensaje);
            Console.WriteLine("INSERTO RECEPTOR: " + result);
        }
        
    }
}
