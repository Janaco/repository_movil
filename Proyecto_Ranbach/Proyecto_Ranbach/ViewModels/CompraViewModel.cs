﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class CompraViewModel : BaseViewModel
    {
        public INavigation navigation { get; set; }
        public ICommand ElegirCmmd { get; set; }

        public ICommand Elegir1 { get; set; }
        public ICommand Elegir2 { get; set; }
        public ICommand Elegir3 { get; set; }
        public ICommand Elegir4 { get; set; }
        public ICommand Elegir5 { get; set; }
        public ICommand Elegir6 { get; set; }
        public ICommand Elegir7 { get; set; }
        public ICommand Elegir8 { get; set; }
        public ICommand Elegir9 { get; set; }
        public ICommand Elegir10 { get; set; }


        public ObservableCollection<CategoriaModel> CategoriaList { get; set; } = new ObservableCollection<CategoriaModel>()
        {
            new CategoriaModel() { IdCategoria=3,Descripcion ="Tecnologia" },
            new CategoriaModel() { IdCategoria=4,Descripcion ="Moda" },
            new CategoriaModel() { IdCategoria=5,Descripcion ="Salud" },
            new CategoriaModel() { IdCategoria=6,Descripcion ="Educación" },
            new CategoriaModel() { IdCategoria=7,Descripcion ="Belleza" },
            new CategoriaModel() { IdCategoria=8,Descripcion ="Mascotas" },
            new CategoriaModel() { IdCategoria=9,Descripcion ="Kids/Bebes" }
        };


        public CompraViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            //ElegirCmmd = new Command<Button>(async (data) => await ElegirAsync(data));

            Elegir1 = new Command(async() => await Elegir1Async());
            Elegir2 = new Command(async () => await Elegir2Async());
            Elegir3 = new Command(async () => await Elegir3Async());
            Elegir4 = new Command(async () => await Elegir4Async());
            Elegir5 = new Command(async () => await Elegir5Async());
            Elegir6 = new Command(async () => await Elegir6Async());
            Elegir7 = new Command(async () => await Elegir7Async());
            Elegir8 = new Command(async () => await Elegir8Async());
            Elegir9 = new Command(async () => await Elegir9Async());
            Elegir10 = new Command(async () => await Elegir10Async());

        }

        async Task Elegir1Async()
        {
            //await navigation.PushAsync(new VW_CompraDetalle(1));
        }

        async Task Elegir2Async()
        {
            //await navigation.PushAsync(new VW_CompraDetalle(1));
        }

        async Task Elegir3Async()
        {
            //await navigation.PushAsync(new VW_CompraDetalle(0));
        }

        async Task Elegir4Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(3, Title = "Tecnologia"));
        }
        async Task Elegir5Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(4, Title = "Moda"));
        }

        async Task Elegir6Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(5, Title = "Salud"));
        }

        async Task Elegir7Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(6, Title = "Educación"));
        }

        async Task Elegir8Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(7, Title = "Belleza"));
        }
        async Task Elegir9Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(8, Title = "Mascotas"));
        }

        async Task Elegir10Async()
        {
            await navigation.PushAsync(new VW_CompraDetalle(9, Title= "Kids/Bebes"));
        }
   

        async Task ElegirAsync(Button data)
        {
            /*
            try {
                int id = 0;
                switch (data.Text)
                {
                    case "Tecnologia":
                        id = 3;
                        break;
                    case "Moda":
                        id = 4;
                        break;
                    case "Salud":
                        id = 5;
                        break;
                    case "Educación":
                        id = 6;
                        break;
                    case "Belleza":
                        id = 7;
                        break;
                    case "Mascotas":
                        id = 8;
                        break;
                    case "Kids/Bebes":
                        id = 9;
                        break;
                    default:
                        break;
                }
                await navigation.PushAsync(new VW_CompraDetalle(id));
            }
            catch (Exception ex) {
                Util.ShortAlerta(Mensaje.Error);
            }*/
          
        }

        
    }
}
