﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class ConfigViewModel: BaseViewModel
    {
        public INavigation navigation { get; set; }
        public UsuarioModel usuario { get; set; }


        public ICommand CuentaCmmd { get; set; }
        public ICommand AvatarCmmd { get; set; }
        public ICommand SoporteCmmd { get; set; }
        public ICommand SalirCmmd { get; set; }
        public ICommand ComunidadCmmd { get; set; }


        public ConfigViewModel(INavigation _navigation)
        {
            navigation = _navigation;
            usuario = App.usuario;

            CuentaCmmd = new Command(async () => await Cuenta());
            AvatarCmmd = new Command(async () => await Avatar());
            SoporteCmmd = new Command(async () => await Soporte());
            ComunidadCmmd = new Command(async () => await Comunidad());
            SalirCmmd = new Command(async () => await Salir());
        }
        async Task Cuenta()
        {
            try
            {
                await navigation.PushAsync(new VW_Dato());
            }
            catch (Exception ex) { }
        }
        async Task Avatar()
        {
            try
            {
                await navigation.PushAsync(new VW_Cuenta(1));
            }
            catch (Exception ex) { }
        }
        async Task Soporte()
        {
            try
            {
                await navigation.PushAsync(new vw_config_soporte());
            }
            catch (Exception ex) { }
        }
        async Task Comunidad()
        {
            try {
                await navigation.PushAsync(new vw_config_comunidad());
            }
            catch (Exception ex) { }
        }

        async Task Salir()
        {
            try
            {
                App.Current.MainPage = new VW_Login();
            }
            catch (Exception ex) { }
        }
    }
}
