﻿using Proyecto_Ranbach.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class MonkeysViewModel : BaseViewModel
    {
        readonly IList<Monkey> source;

        public ObservableCollection<Monkey> Monkeys { get; private set; }


        public MonkeysViewModel()
        {
            source = new List<Monkey>();
            CreateMonkeyCollection();
            
        }
        

        public async Task<UsuarioModel> buscarUsuarioxNombreUsuario(string nombreUser)
        {
            return ( await UsuarioWs.ObtenerUsuarioxNombreUsuario(nombreUser.Trim()) ).usuario;
        }

        public void agregarContacto(Monkey item)
        {

            Monkeys.Add(item);

        }

        void CreateMonkeyCollection()
        {
            source.Add(new Monkey
            {
                Name = "0.99",
                Location = "Notificaciones",
                Details = "Ernesto",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Papio_anubis_%28Serengeti%2C_2009%29.jpg/200px-Papio_anubis_%28Serengeti%2C_2009%29.jpg"
               
            });

            source.Add(new Monkey
            {
                Name = "9.99",
                Location = "Cuenta",
                Details = "David",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Capuchin_Costa_Rica.jpg/200px-Capuchin_Costa_Rica.jpg"
            });

            source.Add(new Monkey
            {
                Name = "49.99",
                Location = "Servicio al Cliente",
                Details = "Micaela",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/BlueMonkey.jpg/220px-BlueMonkey.jpg"
            });

            source.Add(new Monkey
            {
                Name = "99.99",
                Location = "Lenguaje",
                Details = "Denis",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Saimiri_sciureus-1_Luc_Viatour.jpg/220px-Saimiri_sciureus-1_Luc_Viatour.jpg"
            });

            source.Add(new Monkey
            {
                Name = "999.99",
                Location = "Comunidad",
                Details = "Javier",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Golden_lion_tamarin_portrait3.jpg/220px-Golden_lion_tamarin_portrait3.jpg"
            });

          


            Monkeys = new ObservableCollection<Monkey>(source);
        }


        #region INotifyPropertyChanged
        //public event PropertyChangedEventHandler PropertyChanged;

        //void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
        #endregion
    }
}
