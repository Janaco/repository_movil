﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Proyecto_Ranbach.ViewModels
{
    public class LoginViewModel: BaseViewModel
    {
        string msgValue = "";
        public UsuarioModel usuario { get; set; }
        public ICommand IngresarCmmd { get; set; }
        public ICommand RegistarCmmd { get; set; }
        public INavigation navigation{ get; set; }

        public LoginViewModel(UsuarioModel _usuario, INavigation _navigation)
        {
            navigation = _navigation;
            usuario = _usuario;
            IngresarCmmd = new Command(async () => await Ingresar());
            RegistarCmmd = new Command(async () => await Registrar());
        }

        async Task Ingresar()
        {
            try {
                if (ValidaDato())
                {
                    if (Util.CheckForInternetConnection())
                    {
                        
                        var resultado = await UsuarioWs.Login(usuario);
                        
                        if (resultado.codigo.Equals(0))
                        {
                            App.usuario = resultado.usuario;
                            App.Current.MainPage = new NavigationPage(new VW_Principal());
                        }
                        else Util.ShortAlerta(Mensaje.login);
                        
                    }
                }
                else Util.ShortAlerta(msgValue);
            }
            catch (Exception ex) {
                Util.ShortAlerta(Mensaje.Error);
            } 
        }

        async Task Registrar()
        {
            await PopupNavigation.PushAsync(new VW_Social());
        }

        bool ValidaDato()
        {
            msgValue = "";
            if (String.IsNullOrEmpty(usuario.Correo))
            {
                msgValue = Mensaje.correo;
                return false;
            }
            if (String.IsNullOrEmpty(usuario.Contrasenia))
            {
                msgValue = Mensaje.contrasenia;
                return false;
            }

            if (!Util.CheckEmail(usuario.Correo))
            {
                msgValue = Mensaje.formatoMail;
                return false;
            }
            return true;
        }
    }
}
