﻿using System;
namespace Proyecto_Ranbach.Config
{
    public static class Constante
    {
        
        public const string buscar = "Buscar";
        public const string nombreUsuario = "Nombre de Usuario: ";
        public const string aceptar = "Aceptar";
        public const string cancelar = "Cancelar";
        public const string receivePrivateMessage = "ReceivePrivateMessage";
        public const string sendPrivateMessage = "SendPrivateMessage";
    }
}
