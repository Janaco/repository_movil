﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Plugin.Media;
using Proyecto_Ranbach.Interface;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Config
{
    public class Util
    {
        public static void ShortAlerta(string body)
        {
            DependencyService.Get<IMessage>().ShortAlert(body);
        }

        public static void LongAlerta(string body)
        {
            DependencyService.Get<IMessage>().LongAlert(body);
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                ShortAlerta(Mensaje.internet);
                return false;
            }
        }

        public static async Task<bool> CheckCamera()
        {
            try {
                await CrossMedia.Current.Initialize();
                if(!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    Util.ShortAlerta(Mensaje.camara);
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            return true;
        }

        public static async Task<bool> CheckGalery()
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    Util.ShortAlerta(Mensaje.galeria);
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool CheckEmail(string email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
