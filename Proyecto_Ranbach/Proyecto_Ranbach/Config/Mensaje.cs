﻿using System;
namespace Proyecto_Ranbach.Config
{
    public static class Mensaje
    {
        public const  string usuario = "Ingrese usuario";
        public const string contrasenia = "Ingrese contraseña";
        
        public const string login = "Datos incorrectos";

        public const string nombre = "Ingrese nombre";
        public const string apellidos = "Ingrese apellidos";
        public const string correo = "Ingrese correo";
        public const string correoAlternativo = "Ingrese correo alternativo";
        public const string correoDistinto = "El correo alternativo debe ser distinto";
        public const string dni = "Ingrese dni";

        public const string internet = "Verificar conexion de internet";
        public const string camara = "Permiso de Camara";
        public const string formatoMail = "Formato de correo incorrecto";
        public const string galeria = "Permiso de Galeria de fotos";
        public const string usuarioNoDisponible = "Usuario no encontrado";
    
        //Errores
        public const string Error = "Ocurrio un error, intente mas tarde";
        public const string Servidor = "Ocurrio un error, ntente mas tarde";

        public const string ErrorFirebase = "Este dispositivo no es compatible con los servicios de Google Play";
    }
}
