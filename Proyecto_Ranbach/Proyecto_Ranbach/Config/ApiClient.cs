﻿using System;
namespace Proyecto_Ranbach.Config
{
    public static class ApiClient
    {
        public const string nameDB = "gadsDB.db3";

        public const string dominio = "http://proyectos2020-001-site1.dtempurl.com";

        public const string type =  "application/json";

        public const string api = dominio + "/api/";

        //usuario
        public const string login = api + "usuarios/login";

        public const string usuarioNuevo = api + "usuarios/";

        public const string usuarioEditar = api + "usuarios/{0}";

        public const string obtenerUsuarioxNombreUsuario = api + "usuarios/getUserxNombreUser/{0}";

        //Contacto
        public const string agregarContacto = api + "contactos";
        public const string listarContacto = api + "contactos/getContactosxUsuario/{0}";
        public const string eliminarContacto = api + "contactos/{0}"; //ID DEL REGISTRO DEL CONTACTO

        //producto
        public const string producto = api + "productos/{0}";

        public const string productoPorCategoria = api + "productos/categoria/{0}";

        public const string categoria = api + "categorias";

        //Chat
        public const string chathub = dominio + "/chathub";


    }
}
