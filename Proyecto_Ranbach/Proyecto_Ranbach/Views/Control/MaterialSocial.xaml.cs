﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views.Control
{
    public partial class MaterialSocial : ContentView
    {
        //public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(MaterialSocial), defaultBindingMode: BindingMode.TwoWay);
        public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(MaterialSocial), defaultValue: string.Empty, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var matEntry = (MaterialSocial)bindable;
            matEntry.TituloLbl.Text = (string)newVal;
        });

        public static BindableProperty AccentColorProperty = BindableProperty.Create(nameof(AccentColor), typeof(Color), typeof(MaterialSocial), defaultValue: Color.Accent);

        public static BindableProperty ImageProperty = BindableProperty.Create(nameof(Image), typeof(string), typeof(MaterialSocial), defaultValue: string.Empty, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var matEntry = (MaterialSocial)bindable;
            matEntry.IconImage.Source = (string)newVal;
        });

        public Color AccentColor
        {
            get
            {
                return (Color)GetValue(AccentColorProperty);
            }
            set
            {
                SetValue(AccentColorProperty, value);
            }
        }


        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public string Image
        {
            get
            {
                return (string)GetValue(ImageProperty);
            }
            set
            {
                SetValue(ImageProperty, value);
            }
        }

        public MaterialSocial()
        {
            InitializeComponent();
            boxview1.BindingContext = this;
         }
    }
}
