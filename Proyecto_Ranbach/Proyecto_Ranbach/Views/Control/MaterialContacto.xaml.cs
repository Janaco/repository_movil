﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views.Control
{
    public partial class MaterialContacto : ContentView
    {
        public static BindableProperty TituloProperty = BindableProperty.Create(nameof(Titulo), typeof(string), typeof(MaterialContacto), defaultBindingMode: BindingMode.TwoWay, propertyChanged: (bindable, oldVal, newval) =>
        {
            var matEntry = (MaterialContacto)bindable;
            matEntry.tituloLbl.Text = (string)newval;
        });

        public static BindableProperty MensajeProperty = BindableProperty.Create(nameof(Mensaje), typeof(string), typeof(MaterialContacto), defaultBindingMode: BindingMode.TwoWay, propertyChanged: (bindable, oldVal, newval) =>
        {
            var matEntry = (MaterialContacto)bindable;
            matEntry.mensajeLbl.Text = (string)newval;
        });
   
        public string Titulo
        {
            get
            {
                return (string)GetValue(TituloProperty);
            }
            set
            {
                SetValue(TituloProperty, value);
            }
        }

        public string Mensaje
        {
            get
            {
                return (string)GetValue(MensajeProperty);
            }
            set
            {
                SetValue(MensajeProperty, value);
            }
        }

        public MaterialContacto()
        {
            InitializeComponent();
            BindingContext = this; 
        }

      
    }
}
