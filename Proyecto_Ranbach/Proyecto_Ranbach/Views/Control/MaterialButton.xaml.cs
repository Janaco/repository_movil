﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Proyecto_Ranbach.Views.Control
{
    public partial class MaterialButton : ContentView
    {
        public static void Init() { }
        public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(MaterialButton), defaultBindingMode: BindingMode.TwoWay);

        //public static BindableProperty AccentColorProperty = BindableProperty.Create(nameof(AccentColor), typeof(Color), typeof(MaterialButton), defaultValue: Color.Accent);
        /*public static BindableProperty AccentColorProperty = BindableProperty.Create(nameof(AccentColor), typeof(Color), typeof(MaterialButton), defaultValue: string.Empty, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var matEntry = (MaterialButton)bindable;
            matEntry.fondoFm.BackgroundColor = (Color)newVal;
        }); */

        public static BindableProperty ImageProperty = BindableProperty.Create(nameof(Image), typeof(string), typeof(MaterialButton), defaultValue: string.Empty, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var matEntry = (MaterialButton)bindable;
            matEntry.IconImage.Source = (string)newVal;
        });
        /*
        public Color AccentColor
        {
            get
            {
                return (Color)GetValue(AccentColorProperty);
            }
            set
            {
                SetValue(AccentColorProperty, value);
            }
        }
        */

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        } 

        public string Image
        {
            get
            {
                return (string)GetValue(ImageProperty);
            }
            set
            {
                SetValue(ImageProperty, value);
            }
        }


        public MaterialButton()
        {
            InitializeComponent();
            nameLbl.BindingContext = this;
            //fondoFm.BackgroundColor = AccentColor;
        }
    }
}
