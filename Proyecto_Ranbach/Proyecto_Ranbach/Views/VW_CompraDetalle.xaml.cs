﻿using System;
using System.Collections.Generic;
using Proyecto_Ranbach.ViewModels;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_CompraDetalle : ContentPage
    {
        public VW_CompraDetalle(int _categoria, string _title)
        {
            InitializeComponent();
            Title = _title;
            BindingContext = new CompraDetalleViewModel(Navigation, _categoria);
        }
    }
}
