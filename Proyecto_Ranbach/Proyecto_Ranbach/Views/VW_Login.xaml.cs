﻿
using Proyecto_Ranbach.Models; 
using Proyecto_Ranbach.ViewModels; 
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Login : ContentPage
    { 

        public VW_Login()
        {
            InitializeComponent();
            var usuario = new UsuarioModel();
            BindingContext = new LoginViewModel(usuario, Navigation); 
        }

    }
}
