﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Proyecto_Ranbach.ViewModels;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Social : PopupPage
    {
        public VW_Social()
        {
            InitializeComponent();
            BindingContext = new SocialViewModel();
        }
    }
}
