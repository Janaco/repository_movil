﻿using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Ranbach.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class vw_juegos : ContentPage
    {
        public vw_juegos()
        {
            InitializeComponent();
            BindingContext = new MonkeysViewModel();
        }

        async void OnBotonApuestaClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new vw_juegos_apuesta());
        }

        void OnCollectionViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string current = (e.CurrentSelection.FirstOrDefault() as Monkey)?.Name;
            Console.WriteLine("Standard Numeric Format Specifiers\n");
        }

        void OnJuegoClicked(object sender, EventArgs e)
        {
           
            Console.WriteLine("Standard Numeric Format Specifiers\n");
        }

    }
}