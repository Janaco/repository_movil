﻿using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.ViewModels;
using Proyecto_Ranbach.Views.Contactos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Ranbach.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class vw_contactos : ContentPage
    {
        public vw_contactos()
        {
            InitializeComponent();
            BindingContext = new ContactoViewModel(Navigation, this); 
        }

        async void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            try {
                var view = (((StackLayout)sender).GestureRecognizers);

                var model = (view[0] as TapGestureRecognizer).CommandParameter;

                
                await Navigation.PushAsync(new ChatPage(item: model as ContactoModel) );
             }
            catch (Exception ex) {
                Console.WriteLine("Error: {0}", ex.Message);
            }
        }
    }
}