﻿using Proyecto_Ranbach.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Ranbach.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VW_Juegos2 : ContentPage
    {
        public VW_Juegos2()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.BackgroundImageSource = ImageSource.FromFile("fondoApp.png");
            BindingContext = new JuegosViewModel();
        }

        void OnImageButtonClicked(object sender, EventArgs e)
        {
            DisplayAlert("AVISO", "HIZO CLIC", "OK");
        }
    }
}