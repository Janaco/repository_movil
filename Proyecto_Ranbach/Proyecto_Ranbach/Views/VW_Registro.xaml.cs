﻿using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Registro : PopupPage
    {
        public VW_Registro()
        {
            InitializeComponent();
            //var usuario = new UsuarioModel();
            BindingContext = new RegistroViewModel(new UsuarioModel());
        }
        /*
      protected override void OnAppearing()
      {
          base.OnAppearing();

          HidePopup();
      }


      private async void HidePopup()
      {
          //await Task.Delay(4000);
          await PopupNavigation.Instance.RemovePageAsync(this);
          if (PopupNavigation.Instance.PopupStack.Contains(this))
              await PopupNavigation.Instance.RemovePageAsync(this);
      } */
    }
}
