﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.ViewModels;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Compra : ContentPage
    {
        public VW_Compra()
        {
            InitializeComponent();
            BindingContext = new CompraViewModel(Navigation);
        }
    }
}
