﻿using System;
using System.Collections.Generic;
using Proyecto_Ranbach.ViewModels;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Principal : ContentPage
    {
        public VW_Principal()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new PrincipalViewModel(Navigation);
        }
      
    }
}
