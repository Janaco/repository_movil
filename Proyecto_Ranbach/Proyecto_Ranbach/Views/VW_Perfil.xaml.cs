﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Ranbach.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VW_Perfil : ContentPage
    {
        public VW_Perfil()
        {
            InitializeComponent();
            this.BackgroundImageSource = ImageSource.FromFile("fondoVistaPerfil.png");

        }
    }
}