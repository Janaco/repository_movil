﻿using Proyecto_Ranbach.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Ranbach.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class vw_juegos_apuesta : ContentPage
    {
        public vw_juegos_apuesta()
        {
            InitializeComponent();
            BindingContext = new MonkeysViewModel();
        }
    }
}