﻿using System;
using System.Collections.Generic;
using Proyecto_Ranbach.ViewModels;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Cuenta : ContentPage
    {
        public VW_Cuenta(int identity)
        {
            InitializeComponent(); 
            BindingContext = new CuentaViewModel(identity);
        }
         
    }
}
