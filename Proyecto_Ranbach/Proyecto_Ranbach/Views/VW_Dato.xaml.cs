﻿using System;
using System.Collections.Generic;
using Proyecto_Ranbach.ViewModels;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Views
{
    public partial class VW_Dato : ContentPage
    {
        public VW_Dato()
        {
            InitializeComponent();
            BindingContext = new DatoViewModel(Navigation);
        }
        /*
        async void avatar_Click(System.Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new VW_Cuenta(1));
        } */
    }
}
