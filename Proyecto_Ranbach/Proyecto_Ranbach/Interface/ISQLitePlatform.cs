﻿using System;
using SQLite;

namespace Proyecto_Ranbach.Interface
{
    public interface ISQLitePlatform
    {
        SQLiteConnection GetConnection();
        SQLiteAsyncConnection GetConnectionAsync();
    }
}
