﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proyecto_Ranbach.Models;

namespace Proyecto_Ranbach.Interface
{
    public interface IChatService<T>
    {
        Task<bool> AgregarAsync(T item);
        //Task<bool> EditarAsync(T item);
        //Task<bool> EliminarAsync(T item);
        //Task<T> ObtenerAsync(int id);
        Task<List<T>> ObtenerAsync(ContactoModel item, DateTime fechaInicial, DateTime fechaFinal);
        Task<IEnumerable<T>> ListarAsync();
    }
}
