﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Proyecto_Ranbach.Models;

namespace Proyecto_Ranbach.Interface
{
    public interface IUsuarioWebService<T>
    {
        Task<List<UsuarioModel>> ObtenerUsuario(string id = "");

        Task<DataResult> RegistrarUsuario(UsuarioModel item, bool isAdding);

        Task EliminarUsuario(string id);

        Task<DataResult> Login(UsuarioModel item);


        Task<DataResult> ObtenerUsuarioxNombreUsuario(string nombreUsuario);

        Task<DataResult1> AgregarContacto(ContactoModel item);
        Task<DataResult2> ListarContacto(int id);
    }
}
