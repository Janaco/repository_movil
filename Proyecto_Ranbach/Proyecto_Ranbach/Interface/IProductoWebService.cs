﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Proyecto_Ranbach.Models;

namespace Proyecto_Ranbach.Interface
{
    public interface IProductoWebService<T>
    {
        // Gets all of the Walk Entries from our database.
        Task<List<ProductoModel>> ObtenerProducto(string id = "");

        Task<ObservableCollection<ProductoModel>> ObtenerProductoxCategoria(string id);

        // Saves our Walk Entry to the database.
        Task RegistrarProducto(ProductoModel item, bool isAdding);

        // Deletes a specific Walk Entry from the database.
        Task EliminarProducto(string id);
    }
}
