﻿using System;
using Proyecto_Ranbach.Helpers;
using SQLite;

namespace Proyecto_Ranbach.Models
{
    public class BaseDataObject : ObservableObject
    {
        public BaseDataObject()
        {
            Id = Guid.NewGuid().ToString();
        }

        [PrimaryKey]
        public string Id { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public string AzureVersion { get; set; }
    }
}
