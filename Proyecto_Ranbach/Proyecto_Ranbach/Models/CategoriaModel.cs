﻿using System;
namespace Proyecto_Ranbach.Models
{
    public class CategoriaModel
    {
        public int IdCategoria { get; set; }
        public string Descripcion { get; set; }
    }
}
