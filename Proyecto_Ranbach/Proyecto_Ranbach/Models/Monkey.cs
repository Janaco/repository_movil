﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Ranbach.Models
{
    public class Monkey
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public string ImageUrl { get; set; }
        public string chatToken { get; set; } = "";
        public bool IsFavorite { get; set; }

        public string idUsuario { get; set; } = "";
    }

}
