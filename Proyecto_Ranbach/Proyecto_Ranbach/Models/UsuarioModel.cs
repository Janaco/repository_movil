﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Ranbach.Models
{
    public class UsuarioModel
    {
        public int IdUsuario { get; set; }
        public int IdTipoDocumento { get; set; }
        public string NombreUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NroDocumento { get; set; }
        public string Correo { get; set; }
        public string CorreoAlternativo { get; set; }
        public string Contrasenia { get; set; }
        //public string Fecha { get; set; } = "";
        public int Estado { get; set; }
        public string AccessToken { get; set; }
        public string TokenChat { get; set; }
    }
}
