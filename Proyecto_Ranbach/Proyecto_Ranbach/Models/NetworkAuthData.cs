﻿using System;
namespace Proyecto_Ranbach.Models
{
    public class NetworkAuthData : BaseDataObject
    {
        
        private int _codigo;
        private string _name;
        private string _logo;
        private string _picture;
        private string _background;
        private string _foreground;
        private string _correo;

        public string Password { get; set; }

        public int Codigo
        {
            get { return _codigo; }
            set { _codigo = value; OnPropertyChanged("Codigo"); }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        public string Logo
        {
            get { return _logo; }
            set { _logo = value; OnPropertyChanged("Logo"); }
        }

        public string Picture
        {
            get { return _picture; }
            set { _picture = value; OnPropertyChanged("Picture"); }
        }

        public string Background
        {
            get { return _background; }
            set { _background = value; OnPropertyChanged("Background"); }
        }

        public string Foreground
        {
            get { return _foreground; }
            set { _foreground = value; OnPropertyChanged("Foreground"); }
        }
        public string Correo
        {
            get { return _correo; }
            set { _correo = value; OnPropertyChanged("Correo"); }
        } 

        
    }
}
