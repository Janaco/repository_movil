﻿using System;
using SQLite;

namespace Proyecto_Ranbach.Models
{
    [Table("Chat")]
    public class ChatModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Indexed]
        public int IdLogin { get; set; }
        [Indexed]
        public int IdContacto { get; set; }

        //1 = EMISOR 2= RECEPTOR
        public int Comunicador { get; set; }
        public string Mensaje { get; set; }
        public DateTime Fecha { get; set; }
    }
}
