﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Proyecto_Ranbach.Models
{
    public class DataResult
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public bool success { get; set; }

        public UsuarioModel usuario { get; set; }
        public NivelModel nivel { get; set; }
        public UsuarioNivelModel userxnivel { get; set; }
    }

    public class DataResult1
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public bool success { get; set; }

        public ContactoModel contacto { get; set; }
    }

    public class DataResult2
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public bool success { get; set; }
        public List<ContactoModel> listaContacto { get; set; }

    }
}
