﻿using System;
namespace Proyecto_Ranbach.Models
{
    public class NivelModel
    {
        public int IdNivel { get; set; }
        public string Nivel { get; set; }
    }

    public class UsuarioNivelModel
    {
        public int Experiencia { get; set; }
        public string Token { get; set; }
    }
}
