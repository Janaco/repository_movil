﻿using System;
namespace Proyecto_Ranbach.Models
{
    public class ContactoModel
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public string NombreContacto { get; set; }
        public string Mensaje { get; set; }
        public int IdUsuarioxContacto { get; set; }
        public int Estado { get; set; }
    }
}
