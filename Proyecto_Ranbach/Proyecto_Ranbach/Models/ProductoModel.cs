﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Ranbach.Models
{
    public class ProductoModel
    {
        public int IdProducto { get; set; }
        public string Descripcion { get; set; }
        public string RutaFoto { get; set; }
        public int IdCategoria { get; set; }
        public string FecRegistro { get; set; }
        public string FecModificacion { get; set; }
        public string Token { get; set; }
        public string Estado { get; set; }
    }
}
