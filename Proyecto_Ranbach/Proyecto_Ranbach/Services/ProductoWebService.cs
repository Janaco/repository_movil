﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Interface;
using Xamarin.Forms;
using System.Collections.ObjectModel;

[assembly: Dependency(typeof(Proyecto_Ranbach.Services.ProductoWebService))]
namespace Proyecto_Ranbach.Services
{
    public class ProductoWebService : IProductoWebService<ProductoModel>
    {
        public Task EliminarProducto(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ProductoModel>> ObtenerProducto(string id = "")
        {
            var Items = new List<ProductoModel>();
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = await client.GetAsync(String.Format(ApiClient.producto, id));
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        if (String.IsNullOrEmpty(id))
                        {
                            Items = JsonConvert.DeserializeObject<List<ProductoModel>>(content);
                        }
                        else
                        {
                            var item = JsonConvert.DeserializeObject<ProductoModel>(content);
                            Items.Add(item);
                        }
                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }    
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ObtenerProducto: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return Items;
        }

        public async Task<ObservableCollection<ProductoModel>> ObtenerProductoxCategoria(string id)
        {
            var Items = new ObservableCollection<ProductoModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(String.Format(ApiClient.productoPorCategoria, id));
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        Items = JsonConvert.DeserializeObject<ObservableCollection<ProductoModel>>(content);
                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }
                   
            }
            catch (Exception ex)
            {
                // Catch and output any error messages that have occurred
                Debug.WriteLine("ObtenerProductoxCategoria: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return Items;
        }

        public Task RegistrarProducto(ProductoModel item, bool isAdding)
        {
            throw new NotImplementedException();
        }
    }
}
