﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Interface;
using Xamarin.Forms;
using System.Collections.ObjectModel;

[assembly: Dependency(typeof(Proyecto_Ranbach.Services.UsuarioWebService))]

namespace Proyecto_Ranbach.Services
{
    public class UsuarioWebService: IUsuarioWebService<UsuarioModel>
    { 
        public async Task<List<UsuarioModel>> ObtenerUsuario(string id = "")
        {
            // Declare our WalkEntries Items List Collection to populate resultset
            var Items = new List<UsuarioModel>();
            try
            {
                using (var client = new HttpClient()) {
                   

                    var response = await client.GetAsync(String.Format(ApiClient.usuarioEditar,id));
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();

                        if (String.IsNullOrEmpty(id))
                        {
                            Items = JsonConvert.DeserializeObject<List<UsuarioModel>>(content);
                        }
                        else
                        {
                            var item = JsonConvert.DeserializeObject<UsuarioModel>(content);
                            Items.Add(item);
                        }
                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                } 
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ObtenerUsuario: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return Items;
        }

        public async Task<DataResult> RegistrarUsuario(UsuarioModel item, bool isAdding)
        {
            DataResult data = null;
            try
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage responseMessage;
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, ApiClient.type); 

                    if (isAdding) responseMessage = await client.PostAsync(ApiClient.usuarioNuevo, content);
                    else responseMessage = await client.PutAsync(String.Format(ApiClient.usuarioEditar, item.IdUsuario), content);

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var stringResult = await responseMessage.Content.ReadAsStringAsync();
                        data = JsonConvert.DeserializeObject<DataResult>(stringResult);
                        Debug.WriteLine("WalkEntry Item successfully saved.");
                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                    
                }
                    
            }
            catch (Exception ex)
            {
                Debug.WriteLine("RegisrarUsuario: {0}", ex.Message);
                //if (ex.Message.Equals(Mensaje.ErrorInternet)) Util.ShortAlerta(Mensaje.Internet);
                //else Util.ShortAlerta(Mensaje.Error);
                Util.ShortAlerta(Mensaje.Error);
            }
            return data; 
        }

        public async Task EliminarUsuario(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.DeleteAsync(String.Format(ApiClient.usuarioNuevo, id));
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("WalkEntry Item was successfully deleted.");
                    }
                } 
            }
            catch (Exception ex)
            {
                // Catch and output any error messages that have occurred
                Debug.WriteLine("An error occurred {0}", ex.Message);
            }
        }

        public async Task<DataResult> Login(UsuarioModel item)
        {
            DataResult resultado = null;
            try
            {
                using (var client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, ApiClient.type);

                    var response = await client.PostAsync(ApiClient.login, content);

                    Debug.Print("URI Login -> " + ApiClient.login);

                    if (response.IsSuccessStatusCode)
                    {
                        var stringResult = await response.Content.ReadAsStringAsync();
                        //data = JsonConvert.DeserializeObject<DataResult>(stringResult);

                        resultado = JsonConvert.DeserializeObject<DataResult>(stringResult);

                        if(resultado.usuario != null)
                        {
                            resultado.usuario.AccessToken = resultado.usuario.TokenChat;
                        }
                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ObtenerUsuario: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return resultado;
        }

        public async Task<DataResult> ObtenerUsuarioxNombreUsuario(string nombreUsuario)
        {
            var Items = new DataResult();
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(String.Format(ApiClient.obtenerUsuarioxNombreUsuario, nombreUsuario));
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        Items = JsonConvert.DeserializeObject<DataResult>(content);

                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ObtenerUsuarioxNombreUsuario: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return Items;
        }

        public async Task<DataResult1> AgregarContacto(ContactoModel item)
        {
            var data = new DataResult1();
            try {
                using (var client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, ApiClient.type);

                    var response = await client.PostAsync(ApiClient.agregarContacto, content);
                    if (response.IsSuccessStatusCode)
                    {
                        var stringResult = await response.Content.ReadAsStringAsync(); 
                        data = JsonConvert.DeserializeObject<DataResult1>(stringResult);

                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }
            }
            catch(Exception ex) {
                Debug.WriteLine("ObtenerUsuario: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return data;
        }

        public async Task<DataResult2> ListarContacto(int id)
        {
            var Items = new DataResult2();
            try
            {
                using (var client = new HttpClient())
                {
                    string url = String.Format(ApiClient.listarContacto, id);
                    Debug.Print("ListarContacto URL ->" + url);
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        
                        if(!content.Equals("[]"))
                            Items = JsonConvert.DeserializeObject<DataResult2>(content);

                    }
                    else Util.ShortAlerta(Mensaje.Servidor);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ObtenerUsuarioxNombreUsuario: {0}", ex.Message);
                Util.ShortAlerta(Mensaje.Error);
            }
            return Items;
        }

    }
}
