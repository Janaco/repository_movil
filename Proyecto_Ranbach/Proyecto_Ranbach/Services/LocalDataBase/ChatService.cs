﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Proyecto_Ranbach.Interface;
using Proyecto_Ranbach.Models;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(Proyecto_Ranbach.Services.LocalDataBase.ChatService))]
namespace Proyecto_Ranbach.Services.LocalDataBase
{
    public class ChatService: IChatService<ChatModel>
    {
        private readonly ISQLitePlatform _platform;

        public ChatService(ISQLitePlatform platform)
        {
            try
            {
                _platform = platform;
                var con = _platform.GetConnection();
                //con.DropTable<Usuario>();
                con.CreateTable<ChatModel>();
                con.Close();
            }
            catch (Exception ex)
            {
                Debug.Print("UsuarioData: " + ex.Message);
            }
        }
        public ChatService()
        {
            try
            {
                _platform = DependencyService.Get<ISQLitePlatform>();
                var con = _platform.GetConnection();
                //con.DropTable<Usuario>();
                con.CreateTable<ChatModel>();
                con.Close();
            }
            catch (Exception ex)
            {
                Debug.Print("UsuarioData: " + ex.Message);
            }
        }

        public async Task<bool> AgregarAsync(ChatModel item)
        {
            return (await _platform.GetConnectionAsync().InsertAsync(item)) > 0;
        }

        public async Task<List<ChatModel>> ObtenerAsync(ContactoModel item, DateTime fechaInicial, DateTime fechaFinal)
        {
            return await _platform.GetConnectionAsync().Table<ChatModel>().Where(i =>
                                                                        i.IdLogin == item.IdUsuario &&
                                                                        i.IdContacto == item.IdUsuarioxContacto &&
                                                                        i.Fecha < fechaFinal && i.Fecha > fechaInicial).ToListAsync();
        }

        public Task<IEnumerable<ChatModel>> ListarAsync()
        {
            throw new NotImplementedException();
        }
    }
}
