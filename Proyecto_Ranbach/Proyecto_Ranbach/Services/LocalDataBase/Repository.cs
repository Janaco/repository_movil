﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proyecto_Ranbach.Interface;
using Xamarin.Forms;

namespace Proyecto_Ranbach.Services.LocalDataBase
{
    public class Repository<T> where T : class, new()
    {
        private readonly ISQLitePlatform _platform;
        public Repository(ISQLitePlatform platform)
        {
            _platform = platform;
            var con = _platform.GetConnection();
            con.CreateTable<T>();
            con.Close();
        }
        public Repository()
        {
            _platform = DependencyService.Get<ISQLitePlatform>();
            var con = _platform.GetConnection();
            con.CreateTable<T>();
            con.Close();
        }

        public async Task<bool> AddUserAsync(T item)
        {
            return (await _platform.GetConnectionAsync().InsertAsync(item)) > 0;
        }
        public async Task<bool> UpdateUserAsync(T item)
        {
            return (await _platform.GetConnectionAsync().UpdateAsync(item)) > 0;
        }
        public async Task<bool> DeleteUserAsync(T item)
        {
            return (await _platform.GetConnectionAsync().DeleteAsync(item)) > 0;
        }
        public async Task<IEnumerable<T>> GetUsersAsync()
        {
            return await _platform.GetConnectionAsync().Table<T>().ToListAsync();
        }
    }
}
