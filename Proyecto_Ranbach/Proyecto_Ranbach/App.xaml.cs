﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Proyecto_Ranbach.Services;
using Proyecto_Ranbach.Views;
using Proyecto_Ranbach.Models;
using Proyecto_Ranbach.Views.Control;

namespace Proyecto_Ranbach
{
    public partial class App : Application
    {
        public static string emisor = "1";
        public static string User = "Rendy";
        public static UsuarioModel usuario;
        public static bool isNotificacion = false;

        public App()
        {
            InitializeComponent();
            MainPage = new VW_Perfil(); 
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
