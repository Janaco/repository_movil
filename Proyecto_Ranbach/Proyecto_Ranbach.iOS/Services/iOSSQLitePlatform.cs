﻿using System;
using System.IO;
using Proyecto_Ranbach.Config;
using Proyecto_Ranbach.iOS.Services;
using Proyecto_Ranbach.Interface;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(iOSSQLitePlatform))]
namespace Proyecto_Ranbach.iOS.Services
{
    public class iOSSQLitePlatform: ISQLitePlatform
    {
        private string GetPath()
        {
            var dbName = ApiClient.nameDB;
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");

            if (!Directory.Exists(libraryFolder))
            {
                Directory.CreateDirectory(libraryFolder);
            }


            var path = Path.Combine(libraryFolder, dbName);
            return path;
        }

        public SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(GetPath());
        }
        public SQLiteAsyncConnection GetConnectionAsync()
        {
            return new SQLiteAsyncConnection(GetPath());
        }
    }
}
